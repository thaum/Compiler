#include "thaum.hh"

#include "ast.hh"
#include "parser.hh"

using namespace std;

void die(string message) {
    cout << message << endl;
    exit(1);
}

void showtree(AST * tree) {
    if (! tree) {
        cout << "*** NO TREE ***" << endl;
        return;
    }

    tree->show();

    cout << endl;
}

int main(int argc, char * argv[]) {
    if (argc != 2) die("missing arg");
    ifstream source(argv[1]);
    if (! source) die("can't read source");

    Parser p = Parser(source);
    AST * tree = p.match_operators();
    showtree(tree);
    delete tree;

    while (isspace(source.peek())) source.get();

    p._dump_peeks();
    if (! source.eof()) {
        int N = 40;
        char * buf = new char[N+1];
        source.read(buf, N);
        cout << "remaining text: " << buf;
        if (! source.eof()) cout << "...";
        cout << endl;
    }

    /*
    Tokenizer ts = Tokenizer(source);
    while (Token * t = ts.next()) {
        t->show();
        cout << endl;
    }
    */
}
