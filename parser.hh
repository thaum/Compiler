#ifndef PARSER_HH
#define PARSER_HH

#include "thaum.hh"

#include "ast.hh"
#include "tokenizer.hh"

using namespace std;

class Parser {
public:
    Parser(istream & t) : tokens(t) {
    }

    // NULL return means no match, try next option

    AST * match_parenthesized() {
        string errmsg;
        AST * expr = NULL;
        RecordLiteral * rec = NULL;
        IdentToken * ident = NULL;

        if (! try_match(LPAREN_TOK)) return (AST *) NULL; // no match

        if (try_match(RPAREN_TOK)) return new RecordLiteral(); // empty record

        expr = match_expression();
        if (expr && expr->is_error()) goto match_error;

        if (try_match(RPAREN_TOK)) {
            // parenthesized expression
            if (expr) return expr;
            else {
                errmsg = "expected parenthesized expression";
                goto new_match_error;
            }
        }

        rec = new RecordLiteral();

        if (try_match(COLON_TOK)) goto named_items;

        while (try_match(COMMA_TOK)) {
            rec->add(expr);
            expr = match_expression();
            if (! expr) {
                errmsg = "expected expression in record literal";
                goto new_match_error;
            }
            if (expr->is_error()) goto match_error;
        }

        if (try_match(RPAREN_TOK)) {
            rec->add(expr);
            return rec;
        }

        if (! try_match(COLON_TOK)) {
            if (expr) errmsg = "expected :";
            else errmsg = "1 expected expression"; //TODO clarify
            goto new_match_error;
        }

        named_items:
        ident = dynamic_cast<IdentToken *>(expr);
        if (! ident) {
            errmsg = "expected identifier";
            goto new_match_error;
        }

        expr = match_expression();
        if (! expr) {
            errmsg = "2 expected expression";
            goto new_match_error;
        }
        if (expr->is_error()) goto match_error;

        rec->add(ident->value, expr);
        delete ident;

        while (try_match(COMMA_TOK)) {
            ident = try_match<IdentToken>();
            if (! ident) {
                errmsg = "expected identifier";
                goto new_match_error;
            }

            if (! try_match(COLON_TOK)) {
                errmsg = "expected :";
                goto new_match_error;
            }

            expr = match_expression();
            if (! expr) {
                errmsg = "3 expected expression";
                goto new_match_error;
            }
            if (expr->is_error()) goto match_error;

            rec->add(ident->value, expr);
            delete ident;
        }

        if (try_match(RPAREN_TOK)) return rec;
        else {
            errmsg = "expected )";
            goto new_match_error;
        }

        new_match_error:
        delete expr;
        expr = new ParseError(errmsg);

        match_error:
        delete rec;
        delete ident;
        return expr;
    }

    AST * match_control() {
        ResToken * res = try_match<ResToken>();

        if (! res) return (AST *) NULL;

        if (res->matches(IF_TOK)) {
            AST * cond = match_expression();
            if (! cond) return new ParseError("4 expected expression");
            if (cond->is_error()) return cond;

            if (! try_match(THEN_TOK)) return new ParseError("expected \"then\"");
            AST * t_exp = match_expression();
            if (! t_exp) return new ParseError("5 expected expression");
            if (t_exp->is_error()) return t_exp;

            return new IfExpression(cond, t_exp);
        }
    }

    AST * match_term() {
        AST * expr;

        expr = try_match<IdentToken>();
        if (expr) return expr;

        expr = try_match<SymToken>();
        if (expr) return expr;

        expr = try_match<TextToken>();
        if (expr) return expr;

        expr = try_match<AsciiToken>();
        if (expr) return expr;

        expr = try_match<IntToken>();
        if (expr) return expr;

        return (AST *) NULL;
    }

    AST * match_call_arg() {
        AST * expr;

        expr = match_parenthesized();
        if (expr) return expr;

        expr = match_term();
        if (expr) return expr;

        return (AST *) NULL;
    }

    AST * match_expression() {
        AST * expr;

        expr = match_parenthesized();
        if (expr) goto check_call;

        expr = match_control();
        if (expr) return expr;

        expr = match_term();
        if (expr) goto check_call;

        return (AST *) NULL;

        check_call:
        if (expr->is_error()) return expr;

        AST * arg = match_call_arg();
        if (! arg) return expr;
        if (arg->is_error()) return arg;

        expr = new FunctionCall(expr, arg);
        goto check_call;
    }

    AST * match_operators() {
        AST * expr;

        expr = match_expression();
        if (! expr) return (AST *) NULL;
        if (expr->is_error()) return expr;

        OpToken * op = try_match<OpToken>();
        if (! op) return expr;

        OpSequence * seq = new OpSequence();
        seq->add(expr);

        do {
            seq->add(op);

            expr = match_expression();
            if (! expr) return new ParseError("6 expected expression");
            if (expr->is_error()) return expr;
            seq->add(expr);
        } while (op = try_match<OpToken>());

        return seq;
    }

    void _dump_peeks() {
        tokens._dump_peeks();
    }

private:
    Tokenizer tokens;

    // check the next token for a match, don't consume a non-match
    bool try_match(Token * needle) {
        Token * peeked = tokens.peek();
        if (! peeked) return false;

        bool matched = peeked->matches(needle);
        if (matched) delete tokens.next();
        return matched;
    }

    template<class T>
    T * try_match() {
        Token * peeked = tokens.peek();
        if (! peeked) return (T *) NULL;
        T * temp = dynamic_cast<T *>(peeked);
        if (temp) {
            tokens.next();
            return temp;
        }
        else return (T *) NULL;
    }
};

#endif // PARSER_HH
