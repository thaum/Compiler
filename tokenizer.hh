#ifndef TOKENIZER_HH
#define TOKENIZER_HH

#include "thaum.hh"

#include "ast.hh"

using namespace std;

class Tokenizer {
public:
    Tokenizer(istream & t) : text(t) {
    }

    Token * peek() {
        if (! peeked1) peeked1 = next_token();

        return peeked1;
    }

    Token * peek2() {
        if (! peeked2) {
            if (! peeked1) peeked1 = next_token();
            peeked2 = next_token();
        }

        return peeked2;
    }

    Token * next() {
        if (peeked1) {
            Token * temp = peeked1;
            peeked1 = peeked2;
            peeked2 = (Token *) NULL;
            return temp;
        }
        else return next_token();
    }

    void _dump_peeks() {
        if (peeked1) {
            cout << "peeked values: ";
            peeked1->show();
            if (peeked2) {
                cout << ' ';
                peeked2->show();
            }
            cout << endl;
        }
    }

private:
    istream & text;
    Token * peeked1 = (Token *) NULL;
    Token * peeked2 = (Token *) NULL;
    bool hit_error = false;

    int outdents = 0;
    bool is_line_start = true;
    vector<int> indents = {0};

    Token * process_text() {
        string content;
        int c = text.peek();
        if (c == '"') {
            // text literal
            text.get();   // consume initial "
            while ((c = text.get()) != '"') {
                if (c == -1) return hit_error=true, new TerminationError('"');
                content.push_back(c);
            }

            return new TextToken(content);
        }
        else if (c == '\'') {
            // text literal
            text.get();   // consume initial '
            while ((c = text.get()) != '\'') {
                if (c == -1) return hit_error=true, new TerminationError('\'');
                content.push_back(c);
            }

            return new AsciiToken(content);
        }
        else if (c == '`') {
            // extended identifier
            text.get();   // consume initial `
            while ((c = text.get()) != '`') {
                if (c == -1) return hit_error=true, new TerminationError('`');
                content.push_back(c);
            }

            return new IdentToken(content);
        }
        else if (c == '.') {
            // check for symbol
            char d = text.get();   // consume initial .
            if ((c = text.peek()) != '_' && ! isalpha(c)) {
                // not a symbol
                text.putback(d);
                c = d;
                goto actually_operator;
            }

            // symbol
            while (c == '_' || isalnum(c)) {
                content.push_back(text.get());
                c = text.peek();
            }

            return new SymToken(content);
        }
        else if (c == '_' || isalpha(c)) {
            // standard identifier or reserved word
            while (c == '_' || isalnum(c)) {
                content.push_back(text.get());
                c = text.peek();
            }

            if (RESERVED_WORDS.count(content)) return new ResToken(content);
            else return new IdentToken(content);
        }
        else if (c == '-' || isdigit(c)) {
            // check for integer literal
            bool negative = false;
            if (c == '-') {
                char m = text.get();
                if (isdigit(c = text.peek())) negative = true;
                else {
                    // not an integer literal
                    text.putback(m);
                    c = m;
                    goto actually_operator;
                }
            }

            // integer literal
            int n = 0;
            while (isdigit(c)) {
                n *= 10;
                n += text.get() - '0';
                c = text.peek();
            }

            return new IntToken(negative ? -n : n);
        }
        else if (ispunct(c)) {
            // check punctuation
            actually_operator:

            // separators
            if (SEPARATORS.count(c)) {
                content.push_back(text.get());
                return new SepToken(content);
            }

            // operators
            while (ispunct(c)) {
                if (SEPARATORS.count(c)) break;
                content.push_back(text.get());
                c = text.peek();
            }

            return new OpToken(content);
        }

        // no valid token found
        return (Token *) NULL;
    }

    Token * next_token() {
        if (hit_error) return NULL;

        if (text.peek() == '\t') return hit_error=true, new TabError();

        if (outdents) {
            outdents -= 1;
            return new OutdentToken();
        }

    eat_spaces:

        int nspaces = 0;
        while (text.peek() == ' ') {
            nspaces += 1;
            text.get();
        }

        if (text.peek() == '\t') return hit_error=true, new TabError();

        if (text.peek() == '#') {
            // comment to end of line
            while (text.get() != '\n') {
                if (text.eof()) return NULL;
            }
            is_line_start = true;
            goto eat_spaces;
        }

        if (text.peek() == '(') {
            // check for block comment
            char p = text.get();
            char c = text.peek();
            if (c == '#') {
                // block comment
                int start_octothorpes = 0;
                while (text.peek() == '#') {
                    start_octothorpes += 1;
                    text.get();
                }

            look_for_end:

                while (text.peek() != '#') {
                    text.get();
                    if (text.eof()) return NULL;
                }

                int end_octothorpes = 0;
                while (text.peek() == '#') {
                    end_octothorpes += 1;
                    text.get();
                }
                if (end_octothorpes < start_octothorpes) goto look_for_end;
                if (text.get() != ')') goto look_for_end;

                goto eat_spaces;
            }
            else text.putback(p); // not a block comment
        }

        if (text.peek() == '\n') {
            text.get();
            is_line_start = true;
            goto eat_spaces;
        }

        if (is_line_start) {
            is_line_start = false;

            if (nspaces > indents.back()) {
                // increase indentation
                indents.push_back(nspaces);
                return new IndentToken();
            }
            else if (nspaces < indents.back()) {
                // decrease indentation
                while (nspaces < indents.back()) {
                    outdents += 1;
                    indents.pop_back();
                }

                if (nspaces != indents.back()) return hit_error=true, new IndentationError();

                outdents -= 1;
                return new OutdentToken();
            }
        }

        return process_text();
    }
};

#endif // TOKENIZER_HH
