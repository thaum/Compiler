#ifndef AST_HH
#define AST_HH

#include "thaum.hh"

using namespace std;

struct AST {
    virtual bool is_error() {
        return false;
    }

    virtual bool matches(AST * that) = 0;

    virtual void show() = 0;
    virtual void pprint(ostream & out) = 0;

    void pprint() {
        pprint(cout);
    }
};

struct Token : AST {
};

struct SymToken : Token {
    string value;

    SymToken(string s) : value(s) {
    }

    bool matches(AST * that) {
        SymToken * temp = dynamic_cast<SymToken *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    void show() {
        cout << "symbol:." << value;
    }

    void pprint(ostream & out) {
        out << '.' << value;
    }
};

struct ResToken : Token {
    string value;

    ResToken(string s) : value(s) {
    }

    bool matches(AST * that) {
        ResToken * temp = dynamic_cast<ResToken *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    void show() {
        cout << "reserved-word:" << value;
    }

    void pprint(ostream & out) {
        out << value;
    }
};

struct IdentToken : Token {
    string value;

    IdentToken(string s) : value(s) {
    }

    bool matches(AST * that) {
        IdentToken * temp = dynamic_cast<IdentToken *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    bool needs_ticks() {
        if (value[0] != '_' && ! isalpha(value[0])) return true;

        for (char c : value) {
            if (c != '_' && ! isalnum(c)) return true;
        }

        return false;
    }

    void show() {
        bool ticks = needs_ticks();
        cout << "identifier:";
        if (ticks) cout << '`';
        cout << value;
        if (ticks) cout << '`';
    }

    void pprint(ostream & out) {
        bool ticks = needs_ticks();
        if (ticks) cout << '`';
        cout << value;
        if (ticks) cout << '`';
    }
};

struct TextToken : Token {
    string value;

    TextToken(string s) : value(s) {
    }

    bool matches(AST * that) {
        TextToken * temp = dynamic_cast<TextToken *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    void show() {
        cout << "text-literal:" << '"' << value << '"';
    }

    void pprint(ostream & out) {
        out << '"' << value << '"';
    }
};

struct AsciiToken : Token {
    string value;

    AsciiToken(string s) : value(s) {
    }

    bool matches(AST * that) {
        AsciiToken * temp = dynamic_cast<AsciiToken *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    void show() {
        cout << "ascii-literal:" << '\'' << value << '\'';
    }

    void pprint(ostream & out) {
        out << '\'' << value << '\'';
    }
};

struct IntToken : Token {
    int value;

    IntToken(int n) : value(n) {
    }

    bool matches(AST * that) {
        IntToken * temp = dynamic_cast<IntToken *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    void show() {
        cout << "integer-literal:" << value;
    }

    void pprint(ostream & out) {
        out << value;
    }
};

struct OpToken : Token {
    string value;

    OpToken(string s) : value(s) {
    }

    bool matches(AST * that) {
        OpToken * temp = dynamic_cast<OpToken *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    void show() {
        cout << "operator:" << '"' << value << '"';
    }

    void pprint(ostream & out) {
        out << value;
    }
};

struct SepToken : Token {
    string value;

    SepToken(string s) : value(s) {
    }

    bool matches(AST * that) {
        SepToken * temp = dynamic_cast<SepToken *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    void show() {
        cout << "separator:" << '"' << value << '"';
    }

    void pprint(ostream & out) {
        out << value;
    }
};

struct DentationToken : Token {
};

struct IndentToken : DentationToken {
    bool matches(AST * that) {
        IndentToken * temp = dynamic_cast<IndentToken *>(that);
        if (temp) return true;
        else return false;
    }

    void show() {
        cout << "indent->";
    }

    void pprint(ostream & out) {
        // TODO prettyprint indentation
    }
};

struct OutdentToken : DentationToken {
    bool matches(AST * that) {
        OutdentToken * temp = dynamic_cast<OutdentToken *>(that);
        if (temp) return true;
        else return false;
    }

    void show() {
        cout << "<-outdent";
    }

    void pprint(ostream & out) {
        // TODO prettyprint indentation
    }
};

struct ErrorToken : Token {
    bool is_error() {
        return true;
    }

    void pprint(ostream & out) {
        // TODO how to pretty-print a token error?
    }
};

struct TabError : ErrorToken {
    bool matches(AST * that) {
        TabError * temp = dynamic_cast<TabError *>(that);
        if (temp) return true;
        else return false;
    }

    void show() {
        cout << "*** Tab Error ***";
    }
};

struct IndentationError : ErrorToken {
    bool matches(AST * that) {
        IndentationError * temp = dynamic_cast<IndentationError *>(that);
        if (temp) return true;
        else return false;
    }

    void show() {
        cout << "*** Indentation Error ***";
    }
};

struct TerminationError : ErrorToken {
    char value;

    TerminationError(char c) : value(c) {
    }

    bool matches(AST * that) {
        TerminationError * temp = dynamic_cast<TerminationError *>(that);
        if (temp) return value == temp->value;
        else return false;
    }

    void show() {
        cout << "*** Termination Error " << value << " ***";
    }
};

struct RecordLiteral : AST {
    vector<AST *> positional_items;
    map<string, AST *> named_items;

    void add(AST * item) {
        positional_items.push_back(item);
    }

    void add(string name, AST * item) {
        named_items[name] = item;
    }

    bool matches(AST * rawthat) {
        RecordLiteral * that = dynamic_cast<RecordLiteral *>(rawthat);
        if (! that) return false;

        int size = this->positional_items.size();
        if (that->positional_items.size() != size) return false;
        for (int i=0 ; i<size ; i+=1) {
            AST * thisitem = this->positional_items[i];
            AST * thatitem = that->positional_items[i];
            if (! thisitem->matches(thatitem)) return false;
        }

        size = this->named_items.size();
        if (that->named_items.size() != size) return false;
        for (pair<string, AST *> thisitem : this->named_items) {
            string key = thisitem.first;
            AST * thisvalue = thisitem.second;
            if (that->named_items.count(key) == 0) return false;
            AST * thatvalue = that->named_items[key];
            if (! thisvalue->matches(thatvalue)) return false;
        }

        return true;
    }

    void show() {
        cout << "record:(";
        bool first = true;
        for (AST * item : positional_items) {
            if (first) first = false;
            else cout << ", ";

            item->show();
        }
        for (pair<string, AST *> kv : named_items) {
            if (first) first = false;
            else cout << ", ";

            cout << "field:" << kv.first << '=';
            kv.second->show();
        }
        cout << ')';
    }

    void pprint(ostream & out) {
        out << "(";
        bool first = true;
        for (AST * item : positional_items) {
            if (first) first = false;
            else out << ", ";

            item->pprint(out);
        }
        for (pair<string, AST *> kv : named_items) {
            if (first) first = false;
            else out << ", ";

            out << "field:" << kv.first << '=';
            kv.second->pprint(out);
        }
        out << ')';
    }
};

struct FunctionCall : AST {
    AST * callable;
    AST * argument;

    FunctionCall(AST * c, AST * a) : callable(c), argument(a) {
    }

    bool matches(AST * rawthat) {
        FunctionCall * that = dynamic_cast<FunctionCall *>(rawthat);
        if (! that) return false;

        return this->callable->matches(that->callable)
               && this->argument->matches(that->argument);
    }

    void show() {
        cout << "call:(";
        callable->show();
        cout << " with ";
        argument->show();
        cout << ')';
    }

    void pprint(ostream & out) {
        callable->pprint(out);
        out << ' '; // TODO only insert space if necessary?
        argument->pprint(out);
    }
};

struct IfExpression : AST {
    AST * condition;
    AST * true_expr;

    IfExpression(AST * c, AST * t) : condition(c), true_expr(t) {
    }

    bool matches(AST * rawthat) {
        IfExpression * that = dynamic_cast<IfExpression *>(rawthat);
        if (! that) return false;

        return this->condition->matches(that->condition)
               && this->true_expr->matches(that->true_expr);
    }

    void show() {
        cout << "if:(";
        condition->show();
        cout << " then ";
        true_expr->show();
        cout << ')';
    }

    void pprint(ostream & out) {
        ResToken iftok("if");
        ResToken thentok("then");

        iftok.pprint(out);
        condition->pprint(out);
        thentok.pprint(out);
        true_expr->pprint(out);
    }
};

struct OpSequence : AST {
    vector<AST *> items;

    void add(AST * item) {
        items.push_back(item);
    }

    bool matches(AST * rawthat) {
        OpSequence * that = dynamic_cast<OpSequence *>(rawthat);
        if (! that) return false;

        int size = this->items.size();
        if (that->items.size() != size) return false;
        for (int i=0 ; i<size ; i+=1) {
            AST * thisitem = this->items[i];
            AST * thatitem = that->items[i];
            if (! thisitem->matches(thatitem)) return false;
        }
    }

    void show() {
        cout << "ops:(";
        bool first = true;
        for (AST * item : items) {
            if (first) first = false;
            else cout << ", ";

            item->show();
        }
        cout << ')';
    }

    void pprint(ostream & out) {
        bool first = true;
        for (AST * item : items) {
            if (first) first = false;
            else cout << ' ';
            item->show();
        }
    }
};

struct ParseError : AST {
    string message;

    ParseError(string m="Parse Error") : message(m) {
    }

    bool is_error() {
        return true;
    }

    bool matches(AST * rawthat) {
        ParseError * that = dynamic_cast<ParseError *>(rawthat);
        if (that) return this->message == that->message;
        else return false;
    }

    void show() {
        cout << "*** " << message << " ***";
    }

    void pprint(ostream & out) {
        // TODO how to pretty-print a parse error?
    }
};

set<string> RESERVED_WORDS {"if", "then", "while", "module", "using"};
ResToken * IF_TOK = new ResToken("if");
ResToken * THEN_TOK = new ResToken("then");
ResToken * WHILE_TOK = new ResToken("while");
ResToken * MODULE_TOK = new ResToken("module");
ResToken * USING_TOK = new ResToken("using");

set<char> SEPARATORS {'#', '`', '(', ')', '{', '}', ',', ';', ':'};
SepToken * LPAREN_TOK = new SepToken("(");
SepToken * RPAREN_TOK = new SepToken(")");
SepToken * LBRACE_TOK = new SepToken("{");
SepToken * RBRACE_TOK = new SepToken("}");
SepToken * COMMA_TOK = new SepToken(",");
SepToken * SEMI_TOK = new SepToken(";");
SepToken * COLON_TOK = new SepToken(":");

#endif // AST_HH
