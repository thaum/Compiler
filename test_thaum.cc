#include <iostream>
#include <sstream>

#include <rapidcheck.h>
#include "lest.hpp"

#include "json.hpp"
using json = nlohmann::json;

#include "thaum.hh"
#include "ast.hh"
#include "tokenizer.hh"
#include "parser.hh"

using namespace std;

#define CASE( name ) lest_CASE( specification, name )

static lest::tests specification;

CASE("backtick is not a valid operator character")
{
    istringstream textstream("=`");
    Tokenizer ts = Tokenizer(textstream);
    OpToken t("=");
    EXPECT(ts.next()->matches(& t));
}

CASE("single - is a valid operator")
{
    istringstream textstream("-");
    Tokenizer ts = Tokenizer(textstream);
    OpToken t("-");
    EXPECT(ts.next()->matches(& t));
}

CASE("single . is valid operator")
{
    istringstream textstream(".");
    Tokenizer ts = Tokenizer(textstream);
    OpToken t(".");
    EXPECT(ts.next()->matches(& t));
}

CASE("eof in comment doesn't hang")
{
    istringstream textstream("#");
    Tokenizer ts(textstream);
    while (Token * t = ts.next()) {}

    ts.~Tokenizer();
    textstream.~istringstream();

    new(& textstream) istringstream("(#");
    new(& ts) Tokenizer(textstream);
    while (Token * t = ts.next()) {}

    EXPECT(true);
}

CASE("rapidcheck cases")
{
    bool ret;

    ret = rc::check("no zero-character optokens", [](const string & text) {
        // json j = text;
        // cout << j.dump(-1,' ',false,json::error_handler_t::replace) << endl << flush;
        istringstream textstream(text);
        Tokenizer ts(textstream);
        OpToken emptyop("");
        while (Token * t = ts.next()) {
            // t->show(); cout << endl << flush;
            if (t->matches(& emptyop)) return false;
        }
        return true;
    });
    EXPECT(ret);

    ret = rc::check("no token after error", [](const string & text) {
        istringstream textstream(text);
        Tokenizer ts(textstream);
        bool errseen = false;
        while (Token * t = ts.next()) {
            if (errseen) return false;
            if (t->is_error()) {
                errseen = true;
            }
        }
        return true;
    });
    EXPECT(ret);
}

int main(int argc, char * argv[]) {
    int result = lest::run(specification, argc, argv);
    if (result == 0) cout << "OK" << endl;
    return result;
}
